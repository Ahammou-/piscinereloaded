/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recursive_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ahammou- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/04 16:03:19 by ahammou-          #+#    #+#             */
/*   Updated: 2018/10/04 16:28:04 by ahammou-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_recursive_factorial(int nb)
{
	int result;

	result = 1;
	if (nb < 0)
		result = 0;
	else if (nb == 0 || nb == 1)
		result = 1;
	else
		result = nb * ft_recursive_factorial(nb - 1);
	return (result);
}
