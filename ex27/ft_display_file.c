/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_file.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ahammou- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/08 10:16:08 by ahammou-          #+#    #+#             */
/*   Updated: 2018/10/08 14:42:07 by ahammou-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <unistd.h>

void	ft_display_file(char *file_name)
{
	int		fd;
	char	c;

	fd = open(file_name, O_RDONLY);
	if (fd < 0)
	{
		write(2, "open failed.\n", 13);
		return ;
	}
	while (read(fd, &c, 1))
		write(1, &c, 1);
	close(fd);
}

int		main(int argc, char **argv)
{
	if (argc == 1)
	{
		write(2, "File name missing.\n", 19);
	}
	else if (argc == 2)
	{
		ft_display_file(argv[1]);
	}
	else
		write(2, "Too many arguments.\n", 20);
	return (0);
}
