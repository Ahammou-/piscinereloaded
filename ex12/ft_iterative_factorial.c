/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ahammou- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/04 15:38:37 by ahammou-          #+#    #+#             */
/*   Updated: 2018/10/04 16:17:24 by ahammou-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_iterative_factorial(int nb)
{
	int count;
	int fact;

	count = nb;
	fact = 1;
	if (nb < 0)
		fact = 0;
	else if (nb == 0 || nb == 1)
		fact = 1;
	else
	{
		while (count >= 1)
		{
			fact = fact * (count);
			count--;
		}
	}
	return (fact);
}
